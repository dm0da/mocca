require_relative './test_helper'
require 'date'


X = Mocca.VStruct(:i => Integer, :j => String) 

class TC_Basic_VStruct_Args  < Test::Unit::TestCase
  def setup
    @x=nil
  end
  
  def test_api
    assert_raise(NoMethodError) do X.new end
    assert X.respond_to? :create_or_raise
    assert X.respond_to? :create_and_wrap
  end

  def test_setter_ok
    x = X.create_or_raise
    assert_nothing_raised do
      x.i = 1
      x.j = '1'
    end
    
  end

  def test_setter_type_notok
    x = X.create_or_raise
    assert_raise(Tchae::ArgumentTypeException)  do
      x.j = 1
    end
    assert_raise(Tchae::ArgumentTypeException)  do
      x.i = '1'
    end
    assert_raise(Tchae::ArgumentTypeException)  do
      x.i = false
    end
  end
  
  def test_setter_unknown
    x = X.create_or_raise
    assert_raise(NoMethodError)  do
      x.k = 1
    end
    
  end
  
  def test_create_or_raise_positional_ok
    assert_nothing_raised do
      @x = X.create_or_raise(1,'1')
    end
    assert_kind_of X, @x
    assert_equal 1, @x.i
    assert_equal '1', @x.j
  end
  def test_create_or_raise_keyw_ok
    assert_nothing_raised do
      @x = X.create_or_raise(i: 1, j: '1')
    end
    assert_kind_of X, @x
    assert_equal 1, @x.i
    assert_equal '1', @x.j
  end
  def test_create_or_retwra_positional_ok
    
    assert_nothing_raised do
      @x = X.create_and_wrap(1,'1')     
    end
    assert_kind_of Tchae::ResultWrapper, @x
    assert_nil @x.error
    assert_kind_of X, @x.result
    @x = @x.result
    assert_equal 1, @x.i
    assert_equal '1', @x.j
  end
  
  def test_create_or_retwra_keyw_ok
    
    assert_nothing_raised do
      @x = X.create_and_wrap(i: 1, j: '1')     
    end
    assert_kind_of Tchae::ResultWrapper, @x
    assert_nil @x.error
    assert_kind_of X, @x.result
    @x = @x.result
    assert_equal 1, @x.i
    assert_equal '1', @x.j
  end
    def test_create_or_retor_positional_ok
    
    assert_nothing_raised do
      @x = X.create_valid_or_error(1,'1')     
    end
    assert_kind_of X, @x
    assert_equal 1, @x.i
    assert_equal '1', @x.j
  end
  
  def test_create_or_retor_keyw_ok
    
    assert_nothing_raised do
      @x = X.create_valid_or_error(i: 1, j: '1')     
    end
    assert_kind_of X, @x
    assert_equal 1, @x.i
    assert_equal '1', @x.j
  end
  
  def test_create_or_raise_positional_notok
    assert_raise(Tchae::ArgumentTypeException) do
      @x = X.create_or_raise(1,1)
    end
    assert_nil @x
  
  end
  def test_create_or_raise_keyw_notok
    assert_raise(Tchae::ArgumentTypeException) do
      @x = X.create_or_raise(i: '1', j: '1')
    end
    assert_nil @x
  end
  def test_create_or_retwra_positional_notok
    
    assert_nothing_raised do
      @x = X.create_and_wrap('1','1')     
    end
    assert_kind_of Tchae::ResultWrapper, @x
    assert_nil @x.result
    assert_kind_of Tchae::ArgumentTypeError, @x.error
    
  end
  
  def test_create_or_retwra_keyw_notok
    
    assert_nothing_raised do
      @x = X.create_and_wrap(i: '1', j: '1')     
    end
    assert_kind_of Tchae::ResultWrapper, @x
    assert_nil @x.result
    assert_kind_of Tchae::ArgumentTypeError, @x.error
    
  end
  def test_create_or_retor_positional_notok
    
    assert_nothing_raised  do
      @x = X.create_valid_or_error('1','1')     
    end
    assert_kind_of Tchae::ArgumentTypeError, @x
    
  end
  
  def test_create_or_retor_keyw_notok
    
    assert_nothing_raised do
      @x = X.create_valid_or_error(i: '2', j: '1')     
    end
    assert_kind_of Tchae::ArgumentTypeError, @x
  end
  
end

