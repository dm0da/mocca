module Mocca
  class VStruct
    @isa_wrapper = {}
    def self.is_a_wrapper(modklass)
      return @isa_wrapper[modklass] if @isa_wrapper.key?(modklass)
      @isa_wrapper[modklass] = Tchae.Wrapper.args.positional.expect do
        arg {is_a? modklass }
      end
    end
    def initialize
      @values = {}
    end
  end
  
  
  
  def self.VStruct(**props)
    Class.new(VStruct) do
      @props = props
      wrapper_raise = Tchae.Wrapper
      wrapper_retor = Tchae.Wrapper(::Tchae::Handling::RETURN_RESULT_OR_ERROR  )
      wrapper_retwra = Tchae.Wrapper(::Tchae::Handling::RETURN_WRAPPER  )
      h = {}
      p = []
      props.each{|a,klassmod|
        asym = a.to_sym
        define_method(asym) do 
          @values[asym]
        end
        create_validated_method("#{a}=", VStruct.is_a_wrapper(klassmod)) do |val|   @values[asym] = val    end     
          h[asym] = ->{ is_a? klassmod }
          p << ->{ is_a? klassmod }
      }
      wrapper_raise.args.positional.expect(*p)
      wrapper_retwra.args.positional.expect(*p)
      wrapper_retor.args.positional.expect(*p)
      
      wrapper_raise.args.keyword.expect(**h)
      wrapper_retwra.args.keyword.expect(**h)
      wrapper_retor.args.keyword.expect(**h)
      
      create_validated_singleton_method(:create_or_raise, wrapper_raise) do |*p, **kwparms|
       new(*p, **kwparms)
      end
      create_validated_singleton_method(:create_and_wrap,
                                         wrapper_retwra) do |*p, **kwparms|
        new(*p, **kwparms)
      end
      
      create_validated_singleton_method(:create_valid_or_error,
                                         wrapper_retor) do |*p, **kwparms|
        new(*p, **kwparms)
      end
      
      private_class_method :new
      define_method :initialize do |*p, **kwvals|
        super()
       
        p.zip(props.keys).each{|val, a| @values[a] = val}
        kwvals.each{|a, val|  @values[a] = val }
        
      end
      
    end
    
  end
end  