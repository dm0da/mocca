# Mocca

## Introduction 

Mocca's are validated structures.

## Examples

```ruby
require 'mocca'

X = Mocca::VStruct(:i => Integer, :j => String) 
x = X.create_valid_or_error
x.i = 1
x.j = '1'

Y = Mocca::VStruct(n: String, x: X)

y = Y.create_valid_or_error(x: x, n: '123')
z = Y.create_valid_or_error(x: 1, n: '123')

p y  # ==> #<Y:0x4648 @values={:x=>#<X:0x5ba8 @values={:i=>1, :j=>"1"}>, :n=>"123"}>
p z  # ==> #<Tchae::ArgumentTypeError:0x020344f8>
```
