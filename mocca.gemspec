require_relative 'lib/mocca/version'

D = "Mocca's are validated/typed structures".freeze
F = ['lib/mocca.rb'].freeze
F += Dir.glob('lib/*.rb') + Dir.glob('lib/mocca/*.rb') 

Gem::Specification.new do |s|
  s.name        = 'mocca'
  s.version     = Mocca::VERSION
  s.summary     = "Mocca's are validated/typed structures"
  s.description = D
  s.authors     = ['D.M.']
  s.email       = 'dev@aithscel.eu'
  s.files       = F
  s.add_dependency 'tchae', '~> 0.0.2'
  s.required_ruby_version = '>= 2.4.0'
  s.add_development_dependency 'rake', '~> 12.3'
  s.add_development_dependency 'rubocop', '~> 0.51'
  s.homepage = 'https://gitlab.com/dm0da/mocca'
  s.license = 'MIT'
  s.metadata = {
    "bug_tracker_uri" => "https://gitlab.com/dm0da/mocca/issues",
    "changelog_uri" => "https://gitlab.com/dm0da/mocca/blob/master/CHANGELOG",
    "source_code_uri" => "https://gitlab.com/dm0da/mocca/tree/master",
    "wiki_uri" => "https://gitlab.com/dm0da/mocca/wikis/home"
  }
end
